<?php

interface EquipoFutbol
{
    public function entrenar();
    public function pagarFutbolistas();
    public function renovarJugadores();
}
