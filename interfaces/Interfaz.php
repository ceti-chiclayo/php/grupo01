<?php

interface Interfaz
{
    public function metodo1($argumento1, $argumento2);
    public function metodo2($argumento);
    public function metodo3();
}
