<?php

// Funciones
function darFormatoNombreCompleto($argumento1, $argumento2 = "SIN APELLIDO"){
    // CHANAME SANCHEZ, Rubén Dario
    $nombreCompleto = mb_strtoupper($argumento2).", ". $argumento1;
    return $nombreCompleto;
}