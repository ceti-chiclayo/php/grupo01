<?php

class ElementoBase
{
    public $titulo;
    public $descripcion;
    protected $logros = "Logros Elemento Base";
    public $duracion;
    private $visible = false;

    public function __construct($titulo, $descripcion)
    {
        $this->titulo = $titulo;
        $this->descripcion = $descripcion;
    }

    public function calcularDuracion()
    {
        return "Un texto";
    }
}
