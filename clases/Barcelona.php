<?php

require_once './interfaces/EquipoFutbol.php';

class Barcelona implements EquipoFutbol
{
    public function entrenar()
    {
        return "Entrenar en el estadio";
    }
    public function pagarFutbolistas()
    {
        return "Por deposito o transferencia";
    }
    public function renovarJugadores()
    {
        return "No renovar a mayores de 28 años";
    }
}
