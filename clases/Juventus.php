<?php

require_once './interfaces/EquipoFutbol.php';

class Juventus implements EquipoFutbol
{
    public function entrenar()
    {
        return "Entrenar en sus casas";
    }
    public function pagarFutbolistas()
    {
        return "Por deposito";
    }
    public function renovarJugadores()
    {
        return "No renovar a mayores de 36 años";
    }
}
