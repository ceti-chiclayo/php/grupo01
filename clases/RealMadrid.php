<?php

require_once './interfaces/EquipoFutbol.php';

class RealMadrid implements EquipoFutbol
{
    public function entrenar()
    {
        return "Entrenar en verano";
    }
    public function pagarFutbolistas()
    {
        return "Por transferencia";
    }
    public function renovarJugadores()
    {
        return "No renovar a mayores de 30 años";
    }
}
