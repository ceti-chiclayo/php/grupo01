<?php

require_once './interfaces/EquipoFutbol.php';

class Jugador
{
    // argumento = al equipo al que va ir
    public function transferencia(EquipoFutbol $equipoFutbol)
    {
        return "Transferencia a un equipo";
    }

    public function trasferenciaReal(RealMadrid $realMadrid)
    {
        $realMadrid->entrenar();
        return "Transferencia a Real Madrid";
    }
}
