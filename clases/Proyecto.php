<?php

// require_once './clases/ElementoBase.php';
require_once '../interfaces/Interfaz.php';
require_once '../interfaces/Interfaz2.php';

class Proyecto implements Interfaz, Interfaz2
{
    public $presupuesto;

    public function metodo1($arg, $arg2)
    {
        return $arg + $arg2;
    }

    public function metodo2($arg1)
    {
    }

    public function metodo3()
    {
    }

    public function metodo4()
    {
    }
}
