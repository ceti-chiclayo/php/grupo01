<?php

require_once './clases/ElementoBase.php';

class Trabajo extends ElementoBase
{
    public static $subTitulo = 'Sub titulo por defecto';
    public $salario;
    // public $visible;

    public static function metodoEstatico()
    {
        return "Este es un método estático";
    }

    public function __construct($titulo, $descripcion, $salario)
    {
        parent::__construct($titulo, $descripcion);
        $this->salario = $salario;
        // mas lógica
    }

    public function calcularGanancia(int $sueldoDiario, int $numeroDias)
    {
        return $sueldoDiario * $numeroDias;
        // $ganancia = $this->salario * $this->duracion;
        // return $ganancia;
    }

    public function calcularDuracion()
    {
        echo "Logros: " . $this->logros . "<br>";
        return "Calcula duración desde la clase Trabajo";
    }
}
